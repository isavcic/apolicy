#! /bin/sh
#
# chkconfig: - 60 25
# description: ACL Policy Daemon for Postfix
#
# pidfile: /var/run/apolicy.pid
# processname: twistd

### BEGIN INIT INFO
# Provides:          apolicy
# Required-Start:    $local_fs $remote_fs $network $postfix
# Required-Stop:     $local_fs $remote_fs
# Short-Description: start and stop the ACL Policy Daemon for Postfix
# Description:       ACL Policy Daemon provides extended functionality for
#                    the Postfix MTA.
### END INIT INFO

# Author: Miguel Di Ciurcio Filho <miguel.filho@gmail.com>

# source function library
. /etc/rc.d/init.d/functions

RETVAL=0
prog="ACL Policy Daemon"
SITE_PACKAGES=$(python -c 'from distutils import sysconfig; print sysconfig.get_python_lib()')

# Read configuration variable file if it is present
[ -r /etc/sysconfig/twistd ] && . /etc/sysconfig/twistd

start()
{
    echo -n $"Starting $prog: "
    daemon /usr/bin/twistd -y $SITE_PACKAGES/apolicy/server.py -q \
		--logfile /var/log/apolicy.log --pidfile=/var/run/apolicy.pid
    RETVAL=$?
    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/apolicy
    echo
}

stop()
{
    echo -n $"Stopping $prog: "
    killproc -p /var/run/apolicy.pid /usr/bin/twistd
    RETVAL=$?
    [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/apolicy
    echo
}

reload()
{
    echo -n $"Reloading configuration for $prog: "
    pid=$(pidfproc -p /var/run/apolicy.pid /usr/bin/twistd)
    kill -s HUP $pid &> /dev/null
    RETVAL=$?
    if [ $RETVAL -ne 0 ]; then
        failure
    else
        sleep 1
        pgrep 'twistd' > /dev/null
        RETVAL=$?
        if [ $RETVAL -eq 0 ]; then
            success
        else
            # Process has died while reloading configuration
            rm -f /var/run/apolicy.pid &> /dev/null
            failure
        fi
    fi
    echo
}

case "$1" in
  start)
	start
    ;;
  stop)
    stop
    ;;
  reload)
	reload
	;;
  restart)
	stop
	start
	;;
  status)
    status -p /var/run/apolicy.pid /usr/bin/twistd
    RETVAL=$?
    ;;
  condrestart)
    if [ -f /var/lock/subsys/apolicy ]; then
        stop
        start
        RETVAL=$?
    fi
    ;;
  *)
	echo "Usage: $0 {start|stop|restart|reload|status}"
	exit 1
	;;
esac

exit $RETVAL
