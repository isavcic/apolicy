%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:        ACL Policy Daemon for Postfix
Name:           apolicy
Version:        0.73
Release:        1
URL:            http://www.apolicy.org
Source:         http://http://download.gna.org/apolicy/apolicy-%{version}.tar.gz
License:        GPLv2
Packager:       Miguel Di Ciurcio Filho <miguel.filho@gmail.com>
Group:          System Environment/Daemons
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel
BuildArch:      noarch
Requires:       python-pydns >= 2.3, python-pyspf >= 2.0, python-twisted-core >= 2.4, python-IPy >= 0.53, python-memcached


%description
ACL Policy Daemon communicates with the Postfix MTA using the Policy Delegation Protocol,
implementing an ACL (Access Control List) system. Key features: greylisting with flexible
storage using memory for fast responses or disk for high persistence, SPF validation,
control of messages by day/time, variable message size limits per domain or email,
multiple RBL checking, and various ACLs available to use and combine.
The configuration is simple and intuitive.

%prep
%setup -q -n apolicy-%{version}

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}
/bin/mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d
/bin/mkdir -p $RPM_BUILD_ROOT/var/cache/apolicy
install -c apolicy.init $RPM_BUILD_ROOT/etc/rc.d/init.d/apolicy


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc docs/*.txt LICENSE.TXT CONTRIBUTORS.TXT
# Doc about this %config macro http://www-uxsup.csx.cam.ac.uk/~jw35/docs/rpm_config.html
%config(noreplace) /etc/apolicy/main.conf
%config(noreplace) /etc/apolicy/policy.conf
%attr(0755, root, root) %config /etc/rc.d/init.d/apolicy
%{python_sitelib}/apolicy*
%attr(0700,nobody,nobody) /var/cache/apolicy
%post
/sbin/chkconfig --add apolicy
/sbin/service apolicy start

%changelog
* Fri May 16 2008 Miguel Di Ciurcio Filho <miguel.filho@gmail.com> 0.73-1
- Initial release
