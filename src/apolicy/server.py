# ACL Policy Daemon for Postfix
# http://www.apolicy.org
# Copyright (C) 2006, 2007, 2008 Miguel Di Ciurcio Filho
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import re
import sys
import logging
from apolicy.config import config
from apolicy.base import AclError, ActionError
from apolicy.parser import ParsePolicy, ParsePolicyError
import signal

from twisted.protocols import basic
from twisted.internet import reactor, protocol
from twisted.application import internet, service
# TODO: Use this exception when requiring twisted 8.0
#from twisted.internet.error import ReactorNotRunning

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger("apolicy")
line_regex = re.compile(r'^\s*([^=\s]+)\s*=(.*)$')

# The policy must be loaded before creating a service (forking),
# so it is possible to return an exit code to the system
logger.info("Loading configuration.")
policy = ParsePolicy()
try:
    policy.load()
except (ParsePolicyError, AclError, ActionError) as err:
    logger.error(str(err))
    sys.exit(1)

logger.debug("Done loading configuration.")


class PostfixProtocol(basic.LineReceiver):

    delimiter = '\n'
    postfix = {}

    def lineReceived(self, line):
        if line:
            m = line_regex.match(line)
            if m:
                key = m.group(1)
                value = m.group(2)
                if key == "sender" and value == "":
                    value = "<>"
                self.postfix[key] = value
            else:
                logger.error("ERROR: Could not match line %s" % line)
                self.transport.write("action=DUNNO" + '\n\n')
        else:
            action = self.factory.check(self.postfix)
            self.transport.write("action=" + str(action) + '\n\n')
            self.postfix = {}


class ACLPolicyDaemonFactory(protocol.ServerFactory):

    protocol = PostfixProtocol

    def __init__(self, policy):
        self.policy = policy
        self.access_list = self.policy.access_list

        signal.signal(signal.SIGHUP, self._sighup_handler)

    def _sighup_handler(self, signum, frame):
        self._configure()

    def _configure(self):
        logger.info("Loading configuration.")
        try:
            self.policy.load()
            self.access_list = self.policy.access_list
        except (ParsePolicyError, AclError, ActionError) as err:
            logger.error(str(err))
            reactor.stop()
        else:
            logger.debug("Done loading configuration.")

    def check(self, postfix):

        for access in self.access_list:
            logger.debug('Checking access statement: \'%s\' with action \'%s\' -> \'%s\'' %
                (access, access.action.name, access.action))
            if access.check(postfix):
                logger.info('Access statement %s matched all ACLs' % access)
                logger.info("Action: %s" % access.action)
                return access.action

            logger.debug('Access statement %s did not match all acls'
                % access)

        return self.policy.action_list["default"]


class ACLPolicyDaemonService(service.Service):
# This is just an empty class for future needs

    def __init__(self):
        pass

    def startService(self):
        service.Service.startService(self)


application = service.Application('apolicy', uid=config.getint("main", "uid"), gid=config.getint("main", "gid"))
serviceCollection = service.IServiceCollection(application)

address, port = config.get("main", "listen").split(":")

apolicyd_service = ACLPolicyDaemonService()
apolicyd = internet.TCPServer(int(port), ACLPolicyDaemonFactory(policy), interface=address)

apolicyd_service.setServiceParent(service.IServiceCollection(application))
apolicyd.setServiceParent(service.IServiceCollection(application))
