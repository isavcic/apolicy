# ACL Policy Daemon for Postfix
# http://www.apolicy.org
# Copyright (C) 2006,2007,2008  Miguel Di Ciurcio Filho
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import re
import logging
from configparser import ConfigParser
from apolicy.base import *
from apolicy.config import config

logger = logging.getLogger("apolicy")

ACL_CLASSES = {'sender': AclSender,
             'recipient': AclRecipient,
             'log': AclLog,
             'protocol_name': AclProtocolName,
             'client_name': AclClientName,
             'client_address': AclClientAddress,
             'size': AclSize,
             'day': AclDay,
             'time': AclTime,
             'rbl': AclRBL,
             'spf': AclSPF,
             'greylisting': AclGreylisting,
             'helo_name': AclHeloName,
             'sasl_username': AclSaslUsername,
             'sasl_method': AclSaslMethod,
             'sasl_sender': AclSaslSender,
             'reverse_client_name': AclReverseClientName,
             'country': AclCountry,
             'stress': AclStress,}

class ParsePolicyError(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class ParsePolicy(object):

    path = re.compile("^\/.*")
    counter = 0

    def load_acl(self, name, acl_class, value):

        logger.debug("Loading ACL %s: '%s' -> '%s'" % (name, acl_class, value))

        if acl_class in ACL_CLASSES:
            acl_class = ACL_CLASSES[acl_class]
        else:
            raise ParsePolicyError("Invalid ACL class '%s'. Line #%d." % (acl_class, self.counter))

        if name in self.acl_list:
            logger.debug("Adding to %s: '%s' -> '%s'" % (name, acl_class, value))
            self.acl_list[name].add(acl_class(value))
        else:
            logger.debug("Creating %s: '%s' -> '%s'" % (name, acl_class, value))
            acl = Acl(name)
            acl.add(acl_class(value))
            self.acl_list[name] = acl

    def load_action(self, name, action, text):
        logger.debug("Loading Action: %s -> %s %s" % (name, action, text))

        if not name in self.action_list:
            self.action_list[name] = Action(name, action, text)
        else:
            raise ParsePolicyError("Action '%s' already declared. Line #%d." % (name, self.counter))

    def load_access(self, acls, action_name):

        if action_name in self.action_list:
            access = Access(action=self.action_list[action_name])
            logger.debug("Loading Access statement: %s -> %s" % (acls, action_name))
        elif action_name in self.acl_list:
            # If the action_name is an ACL, then we use the default action
            access = Access(action=self.action_list["default"])
            acls.append(action_name)
            logger.debug("Loading Access statement: %s -> %s" % (acls, "default"))
        else: 
            raise ParsePolicyError("Action '%s' does not exist. Line #%d." % (action_name, self.counter))

        for acl in acls:
            negate = False
            m = re.match("^(!)?(.*)", acl)
            acl_name = m.group(2)
            if m.group(1):
                negate = True

            if acl_name not in self.acl_list:
                raise ParsePolicyError("ACL '%s' does not exist. Line #%d." % (acl_name, self.counter))

            access.add_acl(self.acl_list[acl_name], negate)

        self.access_list.append(access)

    def load_from_file(self, name, acl_class, path):

        logger.debug("Loading ACL '%s' values from file: '%s'" % (name, path))

        try:
            acl_file = open(path, "r")
        except IOError as err:
            raise ParsePolicyError("Unable to open file %s: %s. Line #%d." % (path,
                err, self.counter))
        
        for raw_line in acl_file:
            if re.match("^#.*", raw_line):
                continue

            value = raw_line.strip('\n')

            if not value:
                continue

            self.load_acl(name, acl_class, value)


    def load(self):
        self.acl_list = {}
        self.action_list = {}
        self.access_list = []
        self.action_list["default"] = Action("default", "DUNNO")

        try:
            acl_file = open(config.get("main", "policy_path"), "r")

        except IOError as err:
            raise ParsePolicyError("Unable to open policy.conf: %s" % err)

        for raw_line in acl_file:
            self.counter = self.counter + 1

            if re.match("^#.*", raw_line):
                continue

            line = raw_line.strip('\n').split()

            if not line:
                continue

            if line[0] == "acl":
                if len(line) == 4:
                    if line[2] == "country":
                        # country configuration are optional
                        line.append("")

                if len(line) == 3:
                    if line[2] == "greylisting" or line[2] == "spf" or line[2] == "log":
                        line.append("")
                
                if len(line) == 3:
                    if line[2] == "stress":
                        line.append("")

                if self.path.match(line[3]):
                    self.load_from_file(line[1], line[2], line[3])
                else:
                    self.load_acl(line[1], line[2], " ".join([str(s) for s in line[3:]]))

            if line[0] == "action":
                self.load_action(line[1], line[2], " ".join([str(s) for s in line[3:]]))

            if line[0] == "access":
                if len(line) > 2:
                    self.load_access(line[1:-1], line[-1])
                elif len(line) == 2:
                    # There is only one ACL, so we need to pass a list
                    self.load_access([line[1]], "default")
                else:
                    raise ParsePolicyError("Invalid syntax for 'access'. Line #%d" % self.counter)
