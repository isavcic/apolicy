# ACL Policy Daemon for Postfix
# http://www.apolicy.org
# Copyright (C) 2006, 2007, 2008 Miguel Di Ciurcio Filho
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
import logging
import logging.config
import pwd
from configparser import ConfigParser

config_path = "/etc/apolicy/main.conf"

if "APOLICY_CONFIG_PATH" in os.environ:
    config_path = os.environ["APOLICY_CONFIG_PATH"]

config = ConfigParser()
config.readfp(open(config_path))

logging.config.fileConfig(config_path)

if not config.has_option("main", "user"):
    config.set("main", "user", "nobody")

if not config.has_option("main", "listen"):
    config.set("main", "listen", "127.0.0.1:10001")

if not config.has_option("main", "timeout"):
    config.set("main", "timeout", "5")

if not config.has_option("main", "policy_path"):
    config.set("main", "policy_path", "/etc/apolicy/policy.conf")

user = pwd.getpwnam(config.get("main", "user"))
config.set("main", "uid", str(user[2]))
config.set("main", "gid", str(user[3]))
