# ACL Policy Daemon for Postfix
# http://www.apolicy.org
# Copyright (C) 2006,2007,2008  Miguel Di Ciurcio Filho
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from apolicy.config import config
import re
import os
import stat
import socket
import urllib.request, urllib.error, urllib.parse

try:
    from hashlib import md5
except:
    from md5 import md5

import logging
import time
from IPy import IP
import spf

from DNS.Base import DNSError
import DNS
DNS.DiscoverNameServers()

logger = logging.getLogger("apolicy")

P_STATE = {"CONNECT": 1, "HELO": 2, "EHLO": 2, "MAIL": 3, "RCPT": 4,
    "DATA": 5, "END-OF-MESSAGE": 6}

class ActionError(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class Action(object):
    # Reference: http://www.postfix.org/access.5.html
    OK = "OK"
    REJECT = "REJECT"
    DEFER_IF_REJECT = "DEFER_IF_REJECT"
    DEFER_IF_PERMIT = "DEFER_IF_PERMIT"
    BCC = "BCC"
    DISCARD = "DISCARD"
    DUNNO = "DUNNO"
    FILTER = "FILTER"
    HOLD = "HOLD"
    PREPEND = "PREPEND"
    REDIRECT = "REDIRECT"
    WARN = "WARN"

    def __init__(self, name, action, text=None):

        if not name:
            raise ActionError("Name is empty")

        if not action:
            raise ActionError("Action is empty")

        self.name = name
        self.action = action
        self.text = text

    def __str__(self):
        if self.text:
            return "%s %s" % (self.action, self.text)

        return self.action


class AclError(Exception):
    """
    Error that might happen inside any type of ACL class.
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class Acl(object):
    """
    This wraps ACL types objects.
    """

    def __init__(self, name):
        if not name:
            raise AclError("Name is empty")

        self.name = name
        self.data = []

    def add(self, acl):
        self.data.append(acl)

    def __str__(self):
        return self.name

    def check(self, postfix):
        for acl in self.data:
            if acl.check(postfix):
                return True
        return False

    def content(self):
        return " ".join(["'%s' -> '%s'" % (str(d), d._value) for d in self.data])


class Access(object):

    def __init__(self, action=None):
        self.action = action
        self.acl_list = []

    def add_acl(self, acl, negate=False):

        #logger.debug("Adding ACL '%s'" % acl)
        logger.debug("Adding ACL '%s': '%s'" % (acl, acl.content()))

        self.acl_list.append([acl, negate])

    def check(self, postfix):
        for acl, negate in self.acl_list:
            if acl.check(postfix):
                if negate:
                    return False
            else:
                if not negate:
                    return False

        return True

    def __str__(self):
        s = ""
        for acl, negate in self.acl_list:
            if s:
                s = s + " "

            if negate:
                s = s + "!" + str(acl)
            else:
                s = s + str(acl)

        return s


class AclBase(object):
   pass

class AclStress(AclBase):
    def __init__(self, value):
        self._value = "yes"

    def check(self, postfix):
        logger.debug("Checking if server is overloaded")
        if postfix['stress'].lower() == self._value:
            return True

        return False

class AclCountry(AclBase):
    
    _config = {'http_proxy': None, 
                'api_url': "http://api.hostip.info/country.php?ip=%s", 
                'timeout': None}
    
    def __init__(self, value):
        self._value = value
        self._configure()

        for country in self._value:
            country = country.strip()
            if len(country) != 2 or not country.isalpha():
                raise AclError("Invalid country: %s - Country must be 2 letters (see http://www.hostip.info/)" % country)
		
    def check(self, postfix):
        try:
            if self._config['http_proxy']:
                logger.debug("Setting up http proxy")
                proxy_support = urllib.request.ProxyHandler({"http" : self._config['http_proxy']})
                opener = urllib.request.build_opener(proxy_support)
                urllib.request.install_opener(opener)

            if self._config['timeout']:
                old_timeout = socket.getdefaulttimeout()
                # The Python support for fetching resources from the web is layered. urllib2 uses
                # the httplib library, which in turn uses the socket library.
                socket.setdefaulttimeout(self._config['timeout'])

            client_country = urllib.request.urlopen(self._config['api_url'] % postfix['client_address']).read()
            
            if self._config['timeout']:
                # restore timeout
                socket.setdefaulttimeout(old_timeout)
        except Exception as e:
            logger.error("Could not retreive country code via HTTP API: %s" % e)
            return False

        logger.debug("Checking if client country '%s' is listed in '%s'" % (client_country, str(self._value)))
        if client_country in self._value:
            return True

        return False
    
    def _configure(self):
        if not self._value == "":
            try:
                country, config = self._value.split()
            except ValueError:
                # no configuration values, self._value is the country
                self._value = self._value.upper().split(',')
                logger.debug("Loading '%s' as api_url for country" % self._config['api_url'])
                
            else: 
                self._value = country.upper().split(',')

                if config:
                    for item in config.split(','):
                        try:
                            item_dict = item.split('=')
                        except ValueError:
                            raise AclError("Invalid syntax for option in 'country' ACL: '%s'" % item)
                        
                        if item_dict[0] == 'api_url':
                            api_url = "".join(item_dict[1:])
                            logger.debug("Loading '%s' as api_url for country ACL" % api_url)
                            self._config['api_url'] = api_url

                        elif item_dict[0] == 'http_proxy':
                            http_proxy = "".join(item_dict[1:])
                            logger.debug("Loading '%s' as http proxy for country ACL" % http_proxy)
                            self._config['http_proxy'] = http_proxy

                        elif item_dict[0] == 'timeout':
                            timeout = int("".join(item_dict[1:]))
                            logger.debug("Loading '%d' seconds of timeout for country ACL" % timeout)
                            self._config['timeout'] = timeout
                        else:
                            raise AclError("Invalid option for country ACL: '%s'" % key)

                
class AclProtocolName(AclBase):

    def __init__(self, value):

        value = value.upper()

        if value == "ESMTP" or value == "SMTP" or value == "LMTP":
            self._value = value
        else:
            raise AclError("Invalid value for 'protocol_name' ACL: '%s'" % value)

    def check(self, postfix):
        if postfix["protocol_name"] == self._value:
            return True

        return False


class AclLog(AclBase):

    _config = {'result': False,
                'prefix': 'LOG:',
                'content': ['client_address', 'sender', 'recipient']}

    def __init__(self, value):
        self._value = value

    def check(self, postfix):
        # TODO: Fix this
        if not self._value == "":
            for pair in self._value.split(","):
                key, value = pair.split("=")

                if key == "content":
                    if ":" in value:
                        value = value.split(":")
                    else:
                        value = []
                elif key == "result":
                    if value == 'false':
                        value = False
                    elif value == 'true':
                        value = True
                    else:
                        raise AclError("Invalid value for 'log' ACL, parameter 'result': %s" % value)

                self._config[key] = value

        logger.debug("Logging with: '%s'" % str(self._config))
        message = "%s %s" % (self._config["prefix"], " ".join([postfix[c] for c in self._config["content"]])) 
        logger.info(message)

        return self._config["result"]


class AclGreylisting(AclBase):

    _config = {'backend': 'memory',
                'time': 5*60,
                'lifetime': 1440*60,
                'root': '/var/cache/apolicy',
                'host': '127.0.0.1:11211'}

    def __init__(self, value):
        self._value = value
        self._configure()

    def check(self, postfix):

        if P_STATE[postfix["protocol_state"]] < P_STATE["RCPT"]:
            logger.warning("Unable to validade. Protocol state is %s, \
requires at least 'RCPT'" % postfix["protocol_state"])
            return False

        ip = postfix["client_address"]
        sender = postfix["sender"]
        recipient = postfix["recipient"]

        logger.debug("Checking (%s, %s, %s) in the greylist" % (ip, sender, recipient))
        self._now = int(time.time())
        result = self._query(ip, sender, recipient)
        if result:
            logger.debug("Matched")
            return True

        return False

    def _configure(self):
        if not self._value == "":
            for pair in self._value.split(","):
                key, value = pair.split("=")

                if key == "time" or key == "lifetime":
                    value = int(value)*60

                self._config[key] = value

        if self._config["backend"] == "memory":
            self._greylist = {}
            self._query = self._query_memory
        elif self._config["backend"] == "disk":
            self._query = self._query_disk
            # Perform a sanity check on the root greylisting directory
            try:
                st = os.stat(self._config["root"])
            except Exception as err:
                raise AclError(str(err))

            mode = st[stat.ST_MODE]
            if not stat.S_ISDIR(mode):
                raise AclError("The path '%s' for the greylisting database is not a directory" % \
                self._config["root"])

            if st[stat.ST_UID] != config.getint("main", "uid"):
                raise AclError("The user '%s' is not the owner of '%s'" \
                    % (config.get("main", "user"), self._config["root"]))

            if ((stat.S_IMODE(mode) & 0o700) >> 6) != 7:
                raise AclError("The path '%s' does not have proper permissions for the user '%s', \
please set it to at least 0700" % (self._config["root"], config.get("main", "user")))

        elif self._config["backend"] == "memcached":
            try:
                import memcache
            except ImportError:
                raise AclError("Memcached backend requires the 'memcache' module")

            # For accuracy we turn on debug on memcache
            # module if we are in debug mode
            debug = 0
            if config.get("logger_apolicy", "level") == "DEBUG":
                debug = 1

            self.mc = memcache.Client([self._config["host"]], debug=debug)
            if not self.mc.set("test", "test"):
                raise AclError("Unable to connect to memcached")

            self._query = self._query_memcache
        else:
            raise AclError("Invalid backend type '%s'" % self._config["backend"])

    def _query_memcache(self, ip, sender, recipient):
        key = ip+sender+recipient
        t = self.mc.get(key)

        if t:
            if self._now > (t + self._config["time"]):
                return False
        else:
            if not self.mc.add(key, self._now, time=self._config["lifetime"]):
                logger.error("ERROR: Unable to connect to memcached, returning False")
                return False

        return True

    def _query_memory(self, ip, sender, recipient):
        if (ip, sender, recipient) in self._greylist:

            t = self._greylist[(ip, sender, recipient)]
            if (self._now - t) > self._config["lifetime"]:
                self._greylist[(ip, sender, recipient)] = self._now
            elif self._now > (t + self._config["time"]):
                return False
        else:
            self._greylist[(ip, sender, recipient)] = self._now

        return True

    def _query_disk(self, ip, sender, recipient):
        path = "/".join(ip.split('.')[:2])
        path = "%s/%s/" % (self._config["root"], path)
        digest = md5(ip + sender + recipient).hexdigest()
        digest_path = path + digest

        if not os.path.exists(path):
            try:
                os.makedirs(path)
                f = file(digest_path, "w")
                f.close()
            except Exception as err:
                raise AclError(str(err))

        elif not os.path.exists(digest_path):
            f = file(digest_path, "w")
            f.close()
        else:
            t = os.stat(digest_path)[8]
            if (self._now - t) > self._config["lifetime"]:
                os.utime(digest_path, None)
            elif self._now > (t + self._config["time"]):
                return False

        return True


class AclSPF(AclBase):
    # Reference: http://www.openspf.org/SPF_Record_Syntax
    # ('pass', 'sender SPF authorized')
    # ('fail', 'SPF fail - not authorized')
    # ('neutral', 'access neither permitted nor denied')
    # ('softfail', 'domain owner discourages use of this host')
    # ('none', '')
    # ('temperror', 'SPF Temporary Error: DNS Timeout')
    # ('permerror','')
    spf_results = {'neutral': False, 'softfail': False,
                'none': False, 'fail': True, 'temperror': False,
                'permerror': False, 'pass': False}

    _config = {}

    def __init__(self, value):
        self._value = value
        self._configure()

    def _configure(self):
        if not self._value == "":
            for pair in self._value.split(","):
                key, value = pair.split("=")

                if key in self.spf_results:
                    if value == "reject":
                        self.spf_results[key] = True
                    elif value == "pass":
                        self.spf_results[key] = False
                    else:
                        raise AclError("Invalid value '%s' for parameter '%s'" % (value, key))

    def check(self, postfix):

        if P_STATE[postfix["protocol_state"]] < P_STATE["MAIL"]:
            logger.warning("Unable to continue. Protocol state is %s, requires at least 'MAIL'" % postfix["protocol_state"])
            return False

        triplet = (postfix["client_address"], postfix["sender"], postfix["helo_name"])

        logger.debug("Checking SPF for: %s" % triplet[1])

        # Result is a tuple like ('status', 'message')
        try:
            result = spf.check2(i=triplet[0], s=triplet[1], h=triplet[2])
        except Exception as err:
            logger.warning("Failed to verify SPF record for %s, exception: %s" % (str(triplet), str(err)))
            result = ('none', '')

        logger.debug('SPF result: %s' % str(result))

        return self.spf_results[result[0]]


class AclClientAddress(AclBase):

    def __init__(self, value):
        """
        Example of ACL 'client_address':
        acl machine client_address 192.168.0.10
        acl net1 client_address 192.168.1.0/24
        acl net2 client_address 192.168.2.0/255.255.255.0
        acl net3 client_address 192.168.3.0-192.168.3.255
        """
        try:
            self._value = IP(value)
        except ValueError:
            raise AclError("Invalid value for ACL client_address: '%s'" % value)

    def check(self, postfix):

        client_address = IP(postfix["client_address"])

        logger.debug("Checking client_address %s against %s" % (
                client_address, self._value.strNormal()))

        if client_address in self._value:
            logger.debug("Matched")
            return True

        return False


class AclRBL(AclBase):

    def __init__(self, value):
        """
        Receives an RBL hostname.
        """
        self._value = value
        self.timeout = float(config.get("main", "timeout"))

    def check(self, postfix):

        reverse_ip = IP(postfix["client_address"]).reverseName().replace(".in-addr.arpa.", "")
        hostname = "%s.%s" % (reverse_ip, self._value)
        logger.debug("Checking %s on %s" % (
                postfix["client_address"], self._value))
        try:
            request = DNS.DnsRequest(hostname, timeout=self.timeout)
            result = request.req()
            if len(result.answers) > 0:
                logger.debug("Matched")
                return True

        except (DNSError, IOError, socket.error) as error:
            logger.error("Unable to query %s. DNS Problem. Error: %s"
                % (hostname, error))

        return False


class AclSize(AclBase):

    def __init__(self, value):
        """
        Example of ACL 'size':
        acl 10mb size 10240000
        acl 20mb size 20480000
        """
        try:
            self._value = int(value)
            if self._value <= 0:
                raise ValueError
        except ValueError:
            raise AclError("Invalid value for ACL 'size': %s" % value)

    def check(self, postfix):

        if P_STATE[postfix["protocol_state"]] < P_STATE["MAIL"]:
            logger.warning("Unable to continue. Protocol state is %s, requires at least 'MAIL'" % postfix["protocol_state"])
            return False

        size = int(postfix["size"])
        if size == 0:
            logger.warning("Message size is zero")
                
        logger.debug("Checking if %d is greater than %d" % (size, self._value))

        if size > self._value:
            logger.debug("Matched")
            return True

        return False


class AclTime(AclBase):

    def __init__(self, value):
        """
        Receives a match object with the folloing groups:
        group(1): initial hour
        group(2): initial minute
        group(3): final hour
        group(4): final minute

        Example of ACL 'time':
        acl work time 08:00-17:00
        """
        interval = re.compile("([0-9]{2}):([0-9]{2})-([0-9]{2}):([0-9]{2})")

        self._value = interval.match(value)
        if not self._value:
            raise AclError("Invalid time interval for ACL 'time': %s" % value)

        self.first_time = (int(self._value.group(1))*60) + int(self._value.group(2))
        self.last_time = (int(self._value.group(3))*60) + int(self._value.group(4))

    def check(self, postfix):
        localtime = time.localtime()
        cur_time = (localtime[3]*60) + localtime[4]
        logger.debug("Checking time %d:%d against interval %s" % (localtime[3],
                localtime[4], self._value.group()))

        if  cur_time >= self.first_time and cur_time <= self.last_time:
            logger.debug("Matched")
            return True

        return False


class AclDay(AclBase):

    W_DAY = ["m", "t", "w", "h", "f", "a", "s"]

    def __init__(self, value):
        """
        Receives a string with the characters that represent week days.

        m - Monday
        t - Tuesday
        w - Wednesday
        h - Thursday
        f - Friday
        a - Saturday
        s - Sunday

        Example of ACL 'day':
        acl weekend day sm
        """

        for char in value:
            if not char in self.W_DAY:
                raise AclError("Invalid value for ACL 'day': %s" % value)

        self._value = value

    def check(self, postfix):
        cur_day = time.localtime()[6]
        logger.debug("Checking days %s against %s" % (self._value,
            self.W_DAY[cur_day]))
        if self.W_DAY[cur_day] in self._value:
            logger.debug("Matched")
            return True

        return False


class AclRegex(AclBase):

    key = None
    state = "CONNECT"

    def __init__(self, value):
        """
        We will use the 'search' method instead of 'match', so this
        keeps the same behavior of Postfix's configuration.

        Example of ACLs 'sender', 'recipient' and 'client_name':
        acl bad_sender sender foo@bar.com
        acl bad_domain sender @bar.com
        acl dyn_host client_name (.dynamic.|.dhcp.)
        acl trap recipient trap@
        acl other_trap recipient (sales|contact)@bar.com
        """

        self._value = re.compile(value)

    def check(self, postfix):
        if P_STATE[postfix["protocol_state"]] < P_STATE[self.state]:
            logger.warning("Unable to continue. Protocol state is %s, requires at least '%s'" \
                % (postfix["protocol_state"], self.state))
            return False

        if self._value.search(postfix[self.key]):
            logger.debug("Matched")
            return True

        return False


class AclSender(AclRegex):

    key = "sender"
    state = "MAIL"


class AclClientName(AclRegex):

    key = "client_name"


class AclRecipient(AclRegex):

    key = "recipient"
    state = "RCPT"


class AclHeloName(AclRegex):

    state = "HELO"
    key = "helo_name"


class AclSaslUsername(AclRegex):

    state = "MAIL"
    key = "sasl_username"


class AclSaslMethod(AclRegex):

    state = "MAIL"
    key = "sasl_method"


class AclSaslSender(AclRegex):

    state = "MAIL"
    key = "sasl_sender"


class AclReverseClientName(AclRegex):

    key = "reverse_client_name"
