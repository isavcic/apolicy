# apolicy

ACL Policy Daemon communicates with the Postfix MTA using the Policy Delegation
Protocol, implementing an ACL (Access Control List) system.

Key features:

- greylisting with flexible storage using memory for fast responses or disk for
- high persistence
- SPF validation
- control of messages by day/time
- variable
- message size limits per domain or email
- multiple RBL checking, and 
- various ACLs available to use and combine.

The configuration is simple and intuitive.
