== 0.73 ==
 * DEB and RPM packaging
 * New ''memcached'' backend for the greylisting ACL
 * New ''country'' ACL
 * New ''stress'' ACL

== 0.72 ==
 * New ''protocol_name'' ACL
 * Improved debug log messages
 * In an ''access'' statement the ''action'' is now optional. If there is no ''action'' specified, the ''default'' action is used
 * New ''log'' ACL
 * Reduced the requirement for the ''size'' ACL from smtpd_end_of_data_restrictions to smtpd_sender_restrictions, thanks Manuel Mely for the suggestion
 * Better handling configuration errors

== 0.71 ==
 * Code cleanup
 * Improved init scripts for Debian and Red Hat like distributions
 * Fixed a bug when loading ACL values from a file, thanks Sandra Patricia Casas for the report
 * greylisting ACL now has default values: time=5,lifetime=1440,backend=memory,root=/var/cache/apolicy
 * New ''timeout'' parameter available in main.conf.
 * New ''memcached'' backend for greylisting ACL (experimental)
 * Documentation update

== 0.70 ==
 * Now use main.conf for daemon configuration and policy.conf for ACLs, Acesses and Actions
 * Removed MySQL support for now, it was very limited
 * apolicy is now a rock solid TCP server using Twisted!
 * New greylisting support, and it's an ACL too, check docs for more details
 * New ACLs: helo_name, sasl_user, sasl_sender, sasl_method, reverse_client_name
 * SPF verification is now an ACL, removed no_spf statement
 * Simplified and improved log configuration
 * Complete code refactoring
 * Removed pydns, IPy and pyspf from the source tree, now they should be installed separatedly
 * apolicyd *admin has been removed for now

== 0.64 ==
 * Fixed broken import in apolicyd *admin
 * Fixed typos, and copyright notices
 * Log to syslog using /dev/log by default, thanks to Cassio

== 0.63 ==
 * Fixed client_name missing parameter, closed bug #9385

== 0.62 ==
 * Fixed a list comprehension typo that made apolicy not work with Python 2.3

== 0.61 ==
 * Fixed bug when loading modules
 * Fixed sql script errors

== 0.60 ==
 * Moved apolicy.py from a Postfix directory to /usr/sbin and renamed to 
apolicyd, to avoid confusion with the package name.
 * New MySQL engine (beta, but give it a try!).
 * New administration interface apolicyd *admin, will work on any tty and only
with MySQL engine (beta, but give it a try!).
 * New logging configuration. Now we use Python logging facility.
 * Removed the ' *d' option. For debugging, see apolicyd.conf.
 * Reworked most internals to handle and rise exceptions, instead of methods
returning true or false.
 * Fixed a bug when the value of a client_address ACL is invalid, the program
exited without showing any error. Thanks Ricardson Williams.
 * Improved setup.py, now just one command is needed to install.

== 0.5 ==
 * New ACLs: client_address, client_name, rbl, day and time.
 * Merged into the code the IPy module, bringing a complete IPv4 and IPv6
support for the acl client_address.
 * ACLs sender, recipient and client_name accepts regular expressions,
this new feature makes sender_domain and recipient_domain redundant, so
they were REMOVED. To get that behavior you do: "acl a sender @domain.com".
Great code reduction with this change. ADJUST YOUR CONFIGURATION FILE!
 * Removed more useless debug messages.
 * Now we have a decent code documentation, needs more details, but it is
mutch better than it was.
 * New reserved acl 'all'. Use this acl as a wildcard that always matches,
useful if you want to full disable spf checks, like: 'no_spf all'
 * Configuration of apolicy and the ACLs policy have been splited. This
makes apolicy ready and easier to support different backends now on.
 * Installation process simplified. Now just running `python setup.py install`
will do most of the dirt work.
 * The value of the ACLs can now be a text file, like: "acl a sender /path/file.txt".
Inside the text file there must be one value per line. If anyone want to
migrate all access controls from Postfix, this will facilitate.
 * Huge update on documentation.

== 0.4 ==
 * Updated pyspf to version 2.0 with RFC 4408 support.
 * Small but very efficient cache for SPF results, great speed improvement.
 * Heavy profiling made, from 300k calls to 90k calls with 560 requests.
From 40secs to 4secs ACL tests. But outside DNS queries for SPF still a huge
bottleneck.
 * Removed .has_key() usage in pydns.
 * Removed a lot of useless code, renamed variables to shorter names,
simplification everywhere.
 * Apolicy will try silently to use psyco if available, insane performance gain.
 * Now we load with #!/usr/bin/env python, more compatible across Linux distros and BSDs.
 * Add script collector.py. It only gets the data from Postfix and write to /tmp. This was
done do collect data from contributors to test apolicy better.
 * Removed redundant debug log messages.
 * Improved the data validation sent by Postfix, used little code of
tumgreyspf.

== 0.3 ==
 * SPF support added
 * Due to the new SPF support, pyspf and pydns are merged into our code
 * New statement 'no_spf' to skip SPF check based on acls
 * Fixed spell erros on log messages and comments
 * Configuration file is loaded by default from /etc/apolicy/apolicy.conf,
you can use  *c parameter to change
 * Fixed the logging level configuration, some debug messages were missed
 * Some code cleanup

== 0.2 ==
 * Configuration file now can have comments
 * Internal code reorganization. Now the main code is separeted of the
configuration loading and acl validation
 * Lots of changes in comments and variable names for better reading
 * Ajust code to conform with official Python style (PEP 8)
 * New action behavior, the default action can be changed
 * Better exit handling in case of problems

== 0.1 ==
 * Initial release
