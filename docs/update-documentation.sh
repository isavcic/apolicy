#!/bin/sh

USER_AGENT="User-agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.11) Gecko/20071128 Iceweasel/2.0.0.11 (Debian-2.0.0.11-1)"

URL="http://www.apolicy.org/cgi-bin/moin.cgi"


wget --header="$USER_AGENT" $URL/Tutorial?action=raw -O tutorial.txt
wget --header="$USER_AGENT" $URL/AclList?action=raw -O acl-list.txt
wget --header="$USER_AGENT" $URL/Installation?action=raw -O installation.txt
wget --header="$USER_AGENT" $URL/Configuration?action=raw -O configuration.txt
wget --header="$USER_AGENT" $URL/Changelog?action=raw -O changelog.txt
